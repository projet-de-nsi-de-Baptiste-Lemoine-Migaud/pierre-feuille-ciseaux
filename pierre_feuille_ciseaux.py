from random import *
 
def choix_utilisateur(): #Cette fonction permet au joueur de choisir son mouvement (pierre,feuille ou ciseaux)
    joueur = input("Pierre\nPapier\nCiseaux\n") #La machine nous ecrit les trois possibilités de mouvement
    while joueur not in ["pierre","papier","ciseaux"]: #Si le joueur met quelque chose d'autre que pierre, feuille ou ciseaux :
        joueur = input("pierre\npapier\nciseaux\n") #Ca remet pierre, feuille ou ciseaux jusqu'a ce que la joueur mette un des trois mouvements
    return(joueur) #renvoie le choix du joueur
 
def choix_ordinateur(): #Cette fonction permet à l'ordinateur de générer aléatoirement un mouvement
    n = randint(1,3) #Prend un nombre aléatoire entre 1 et 2 car 3 n'est pas compris
    if n == 1:
        ordi = "pierre"
    elif n==2:
        ordi = "papier"  #-> Choix aléatoire de l'ordinateur
    else:
        ordi = "ciseaux"
    return(ordi) #renvoie le choix de l'ordinateur
 
a = choix_utilisateur() 
b = choix_ordinateur()
print(b)
if a == "pierre" and b == "ciseaux":   #
    print("Vous avez gagne.")          #
elif a == "pierre" and b == "papier":  #
    print("Vous avez perdu.")          #
elif a == "pierre" and b == "pierre":  #
    print("Egalite.")                  #
                                       #
if a == "papier" and b == "pierre":    #
    print("Vous avez gagne.")          #
elif a == "papier" and b == "ciseaux": # Pour comprendre cela voir 'l'introduction' dans
    print("Vous avez perdu.")          # laquelle il y a les règles du P,F,C
elif a == "papier" and b == "papier":  #
    print("Egalite.")                  #
                                       #
if a == "ciseaux" and b == "papier":   #
    print("Vous avez gagne.")          #
elif a == "ciseaux" and b == "pierre": #
    print("Vous avez perdu.")          #
elif a == "ciseaux" and b == "ciseaux":#
    print("Egalite.")                  #